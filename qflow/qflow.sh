#!/bin/bash

ln -sfv ../core/rtl/verilog source

/usr/lib/qflow/scripts/synthesize.sh $PWD openMSP430 || exit 1
/usr/lib/qflow/scripts/placement.sh -d $PWD openMSP430 || exit 1
# /usr/lib/qflow/scripts/vesta.sh $PWD openMSP430 || exit 1
/usr/lib/qflow/scripts/router.sh $PWD openMSP430 || exit 1
# /usr/lib/qflow/scripts/placement.sh -f -d $PWD openMSP430 || exit 1
# /usr/lib/qflow/scripts/router.sh $PWD openMSP430 $status || exit 1
# /usr/lib/qflow/scripts/cleanup.sh $PWD openMSP430 || exit 1
# /usr/lib/qflow/scripts/display.sh $PWD openMSP430 || exit 1
